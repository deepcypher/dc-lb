DeepCypher External Load Balancer
=================================

During bootstrapping of a highly available Kubernetes cluster it is necessary to provision an external load balancer pointing to the respective kube-api servers. We used to rely on the in-built nginx server of OpenWRT that was reconfigured to also act as the load balancer of the cluster, however we now want to both alleviate strain on the core networking infrastructure. To this end we provision an external independent load balancer. This repo acts as a hub for a configurable nginx docker script to be easily spun up on whatever machine takes this load balancer role.

Templates!
----------

Nginx 1.19 now supports using a template directory. `/etc/nginx/templates`
all files in the template directory will be placed in `/etc/nginx/templates` inside the container, whereby they will be templated. Nginx does this specifically in the docker container using the shell env builtin substitution called `envsubst`

FROM nginx official documentation

  - NGINX_ENVSUBST_TEMPLATE_DIR
        A directory which contains template files (default: /etc/nginx/templates)
        When this directory doesn't exist, this function will do nothing about template processing.

  - NGINX_ENVSUBST_TEMPLATE_SUFFIX
        A suffix of template files (default: .template)
        This function only processes the files whose name ends with this suffix.

  - NGINX_ENVSUBST_OUTPUT_DIR
        A directory where the result of executing envsubst is output (default: /etc/nginx/conf.d)
        The output filename is the template filename with the suffix removed.
            ex.) /etc/nginx/templates/default.conf.template will be output with the filename /etc/nginx/conf.d/default.conf.
        This directory must be writable by the user running a container.

