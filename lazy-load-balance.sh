#!/usr/bin/env bash
docker build -t a/dc-lb -f Dockerfile . && \
# docker run -detach --restart=always -p 6443:6443 -it a/dc-lb
docker run -p 6443:6443 --name kube_api_lb --rm -it a/dc-lb
