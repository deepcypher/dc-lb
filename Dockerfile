FROM nginx
COPY nginx.conf /etc/nginx/nginx.conf
COPY templates /etc/nginx/templates

EXPOSE 6443/tcp
