#!/usr/bin/env bash
echo "Updating container to not restart:"
docker update --restart=no kube_api_lb
echo "Stopping container"
docker stop kube_api_lb
echo "Removing container"
docker rm kube_api_lb
