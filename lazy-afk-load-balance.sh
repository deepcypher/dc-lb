#!/usr/bin/env bash
docker build -t a/dc-lb -f Dockerfile . && \
docker run -detach --restart=always --name kube_api_lb -p 6443:6443 -it a/dc-lb
# docker run -p 6443:6443 -it a/dc-lb
